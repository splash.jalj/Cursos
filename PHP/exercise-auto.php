<? php

class Auto{
var $color;
var $numPuertas;
var $marca;	

	function __construct($color, $numPuertas, $marca) {

		$this->color = $color;
		$this->numPuertas = $numPuertas;
		$this->marca = $marca;
	}

	function Acelerar() {
		return 'Puede acelerar';
	}
	function TocarBocina() {
		return 'Puede tocar la bocina';
	}
	function caracteristica(){
		return $this->color. ',' . $this->numPuertas.','. $this->marca;
	}

}
	$ultimate = new Auto('Rojo','4','Tesla');

	echo "El auto con las siguientes características: {$ultimate->caracteristica()} puede realizar las siguientes acciones: {$ultimate->Acelerar()} y también: {$ultimate->TocarBocina()}.";